from flask import Flask, render_template
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast
import uuid

app = Flask(__name__)
api = Api(app)

cursor = 1

class Users(Resource):
    def get(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('latest', required=False)
        args = parser.parse_args()  # parse arguments to dictionary

        data = pd.read_csv('users.csv')  # read local CSV
        data = data.to_dict('records')  # convert dataframe to dict
        if args['latest']:
            global cursor
            if cursor < len(data):
                print(len(data) - cursor)
                return_data = data[len(data) - cursor]
                cursor += 1
                return return_data, 200
            else:
                cursor = 1
                return data[0], 200
        else:
            return data, 200  # return data and 200 OK

    def post(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('name', required=True)
        parser.add_argument('status', required=False, default='intro')
        parser.add_argument('userId', required=False, default ='')
        args = parser.parse_args()  # parse arguments to dictionary
        print(args)

        # read our CSV
        data = pd.read_csv('users.csv')

        if args['userId'] in list(data['userId']):
            return {
                'message': f"'{args['userId']}' already exists."
            }, 409
        else:
            # create new dataframe containing new values
            new_data = pd.DataFrame({
                'userId': [str(uuid.uuid1())],
                'name': [args['name']],
                'status': [args['status']]
            })
            # add the newly provided values
            data = data.append(new_data, ignore_index=True)
            data.to_csv('users.csv', index=False)  # save back to CSV
            return {'data': data.to_dict()}, 200  # return data with 200 OK

    def put(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('userId', required=True)  # add args
        parser.add_argument('location', required=True)
        args = parser.parse_args()  # parse arguments to dictionary

        # read our CSV
        data = pd.read_csv('users.csv')
        
        if args['userId'] in list(data['userId']):
            # evaluate strings of lists to lists !!! never put something like this in prod
            data['locations'] = data['locations'].apply(
                lambda x: ast.literal_eval(x)
            )
            # select our user
            user_data = data[data['userId'] == args['userId']]

            # update user's locations
            user_data['locations'] = user_data['locations'].values[0] \
                .append(args['location'])
            
            # save back to CSV
            data.to_csv('users.csv', index=False)
            # return data and 200 OK
            return {'data': data.to_dict()}, 200

        else:
            # otherwise the userId does not exist
            return {
                'message': f"'{args['userId']}' user not found."
            }, 404

    def delete(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('userId', required=True)  # add userId arg
        args = parser.parse_args()  # parse arguments to dictionary
        
        # read our CSV
        data = pd.read_csv('users.csv')
        
        if args['userId'] in list(data['userId']):
            # remove data entry matching given userId
            data = data[data['userId'] != args['userId']]
            
            # save back to CSV
            data.to_csv('users.csv', index=False)
            # return data and 200 OK
            return {'data': data.to_dict()}, 200
        else:
            # otherwise we return 404 because userId does not exist
            return {
                'message': f"'{args['userId']}' user not found."
            }, 404

                    
class Informations(Resource):
    def get(self):
        data = pd.read_csv('infos.csv')  # read local CSV
        return data.to_dict('records'), 200  # return data dict and 200 OK

class Riddles(Resource):
    def get(self):
        data = pd.read_csv('riddles.csv')  # read local CSV
        return data.to_dict('records'), 200  # return data dict and 200 OK

api.add_resource(Users, '/users')  # add endpoints
api.add_resource(Informations, '/infos')
api.add_resource(Riddles, '/riddles')

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0',port=8080)  # run our Flask app