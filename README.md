# tremo

Tremos Webservice that searches for identification Data if you feed it a phone number.

### Intall

`sudo apt install git`

`git clone https://gitlab.com/machina_ex/patrol/tremo.git`

`sudo apt install python3-pip`

`pip3 install flask`

`pip3 install flask_restful`

`pip3 install pandas`

`sudo apt-get install python-scipy`
`sudo apt update`
`sudo apt install libatlas-base-dev`

Append two lines to autostart file

`nano /home/pi/.bashrc`

```
cd /home/pi/tremo
python3 app.py
```

Append 1 line to cronjobs

`crontab -e`

`0 12 * * * cp /home/pi/tremo/users_template.csv /home/pi/tremo/users.csv`