console.log("GO")
let $user_input = $('#user_input')
let $send = $('#send_button')
let $input_row = $('#input_row')
var user_history = []
var riddles = []
var riddle_count = 0
var current_user = ''

$user_input.on('keypress', e => {
    if(e.which === 13){
        $user_input.attr("disabled", "disabled")
        $user_input.trigger("enterKey")
    }
})

$send.on('click', e => {
    $user_input.trigger("enterKey")
})

 async function refugium() {
    user_history = await getData("users")
    riddles = await getData("riddles")
    //let latest_user = await getData("users", {latest:true})
    //console.log(latest_user)
    
    await this.hacker.pause(1000)
    $('#console_line').show()
    await whoSendYou()
    await whoAreYou()
    await whatDoYouWant()
    await whatDoYouKnow()
    $('#console_line').hide()
 }

async function whoSendYou() {
    
    await hacker.pause(5000)
    await hacker.type('Hallo Fremder.')
    await hacker.pause(3000)
    await hacker.type('Willkommen in Tremos Refugium.')
    await hacker.pause(3000)
    await hacker.type('Wer schickt dich?', 1000)
    let success = false

    while(!success) {
        let answer = await awaitInput()
        await hacker.type('', 1000)
        let check = await findData('infos',answer)
        //console.log("found:")
        //console.log(check)

        if(check && check.result == "friend") {
            await hacker.type('Freunde von Kate sind auch meine Freunde.')
            success = true
        } else {
            await hacker.type(`${answer} ist mir nicht bekannt.`)
            await hacker.type('Ich möchte nicht unhöflich erscheinen. Aber Tremos Refugium steht nicht jedem offen.')
            await hacker.type('', 2500)
            await hacker.type('Wer schickt dich also?', 1000)
        }
    }
}

async function whoAreYou() {
    await hacker.type('Wer verlangt nach meinen Diensten?', 1000)
    let success = false
    let answer = ''
    while(!success) {
        answer = await awaitInput()
        await hacker.type('', 1500)
        success = true
        let dishonest = ["ICH","KATE","KATE BISCHOFF","VERONICA","ANGELICA","RICK MILLER","RICK","MILLER","TREMO"]
        dishonest.forEach(elem => {
            if(answer.toUpperCase() == elem) {
                success = false
            }
        })
        if(!success) {
            await hacker.type('Bitte sei ehrlich zu mir. Ich werde auch ehrlich zu dir sein.')
            await hacker.type('Nochmal. Wie ist dein Name?')
        }
    }
    
    await hacker.type(`${answer} ...`)
    current_user = answer
    await hacker.pause(2000)
    await hacker.type(`Ich freue mich deine Bekanntschafft zu machen.`)
}

async function whatDoYouWant() {
    await hacker.type('Was kann ich für dich tun, Freund?')
    
    let start_research = false
    while(!start_research) {
        await hacker.type('Kann ich dir mit einer <b>Recherche</b> behilflich sein oder möchtest du etwas über deine <b>Vorgänger</b> erfahren?', 1000)
        
        let answer = await awaitInput()
        await hacker.type('', 1500)

        if(answer.toUpperCase().includes("RECHERCHE")) {
            await hacker.type('Aber klar.')
            return
        } else if(answer.toUpperCase().includes("VORGÄNG")) {
            if(riddle_count >= riddles.length) {
                await hacker.type('Mir fällt weiter kein Rätsel ein.')
                await hacker.type('Vielleicht gibt es etwas, das du suchst aber nicht finden kannst.')
                break
            }
            await hacker.type('Gut.', 2000)
            await hacker.type('Beantworte mir eine Frage und ich beantworte deine Neugierde.')
            let ridd = riddles[riddle_count]
            await hacker.type(ridd.question, 1500)
            let riddle_answer = await awaitInput()
            await hacker.type('', 1500)

            if(riddle_answer.toUpperCase().includes(ridd.answer1) || riddle_answer.toUpperCase().includes(ridd.answer2)) {
                await hacker.type('Richtig!', 3000)
                await hacker.type('Als Belohnung bekommst du einen Namen von mir.')
                //let reward = user_history[Math.floor(Math.random() * (user_history.length))].name
                let latest_user = await getData('users', {'latest':true})
                let reward = latest_user.name
                await hacker.type(`${reward} war vor dir hier und hat meine Freundschafft in Anspruch genommen.`)
                riddle_count++
            } else {
                await hacker.type('Das sehe ich anders.')
            }
        } else {
            await hacker.type('So gut kennen wir uns nicht. Mehr kann ich dir nicht anbieten.')
        }
    }
}

async function whatDoYouKnow() {
    let success = false
    let miller_count = 0
    await hacker.type('Bitte gib mir einen Anhaltspunkt, wonach ich für dich suchen soll.', 1500)
    while(!success) {
        let clue = await awaitInput()
        await hacker.type('', 1500)
        let finding = await findData('infos', clue)
        await hacker.type('Hm ...', 4000)
        if(finding) {
            switch (finding.result) {
                case "friend":
                    await hacker.type('Wenn du mehr über Kate erfahren willst wird es genügen ihr Fragen zu stellen statt mir.')
                    break
                case "lead":
                    await hacker.type('Ein Allerweltsname. Klingt nach einer Herausforderung!')
                    await hacker.type(`Ich bräuchte eine ergänzende information zu ${clue} um dir weiterzuhelfen.`)
                    await hacker.type(`Eine Telefonnummer oder etwas in der Art.`) // Ist das zu konkret? Muss wohl so oder?
                    break
                case "win":
                    await postData("users",{name:current_user, status:"WIN"})
                    await hacker.type('Sieh an.')
                    await hacker.type(`Die Telefonnummer ist auf "${finding.detail}" registriert.`, 15000)
                    await hacker.type(`Herr ${finding.detail} möchte offensichtlich nicht, dass man ihn mit seinem Konterfeit Rick Miller assoziiert.`)
                    break
                case "try":
                    await hacker.type('Ich denke hier wirst du dir selbst helfen müssen.')
                    await hacker.type(`Nur zu! Du wirst einen Weg finden, mehr über ${finding.detail} zu erfahren.`)
                    break
            }
            await hacker.type('Kann ich noch etwas für dich tun?')
            let more = await awaitInput()
            await hacker.type('', 1500)
            if(more.toUpperCase().includes("JA")) {
                await hacker.type('Gut.', 3000)
                await hacker.type('Gib mir eine Information mit der ich die Suche beginnen kann.', 1500)
            } else if(more.toUpperCase().includes("NEIN")) {
                await hacker.type('Es war mir eine Ehre dich in meinem Refugium zu beherbergen.')
                await hacker.type('Adieu.', 4000)
                await hacker.erase()
                
                success = true
            } else {
                await hacker.type('Nun.', 2000)
                await hacker.type('Teile mir mit, wenn du nach etwas suchst. Vielleicht kann ich dir eine Hilfe sein.')
                await hacker.type('Mit welcher Information soll ich die Suche beginnen?', 1500)
            }
        } else {
            await hacker.type(`Mit > ${clue} < hatte ich leider keinen Erfolg.`)
            await hacker.type('Keine Sorge. Wir werden schon etwas finden.')
            await hacker.type('Mit welcher Information soll ich die Suche jetzt beginnen?', 1500)
        }
    }
}

function awaitInput() {
    return new Promise((resolve, reject) => {
        $input_row.show()
        $user_input.on("enterKey", e => {
            let input = $user_input.children().val()
            if(input != '') {
                $user_input.off("enterKey")
                $input_row.hide()
                $user_input.children().val('')
                $user_input.removeAttr("disabled")
                console.log("Answer " + input)
                return resolve(input)
            }
        })
    })
}

$(document).ready(function() {
    setInterval ('cursorAnimation()', 600)
    refugium()
});

function testTypingEffect() {
    caption = $('input#user-caption').val();
    type();
}

class TypeEffect {
    constructor(element, interval) {
        this.captionEl = $(element)
        this.captionLength = 0;
        this.caption = ''
        this.interval = interval
    }

    pause(length) {
        return new Promise((resolve, reject) => {
            console.log(`pause for ${length} ms`)
            setTimeout(() => {
                return resolve()
            }, length)
        })
    }

    type(caption, pause) {
        return new Promise((resolve, reject) => {
            this.caption = caption
            this.typeAnimation(() => {
                console.log(`done typing: ${caption}`)
                if(typeof pause === 'undefined') {
                    pause = caption.length * 80
                }
                this.pause(pause)
                .then(result => {
                    return resolve()
                })
            })
        })
    }

    typeAnimation(done) {
        this.captionEl.html(this.caption.substr(0, this.captionLength++));
        if(this.captionLength < this.caption.length+1) {
            setTimeout(this.typeAnimation.bind(this), this.interval, done)
        } else {
            this.captionLength = 0;
            this.caption = '';
            done()
        }
    }

    erase(pause) {
        return new Promise((resolve, reject) => {
            let caption = this.captionEl.html()
            this.caption = caption
            this.captionLength = this.caption.length
            if (this.captionLength>0) {
                this.eraseAnimation(() => {
                    console.log(`done erasing: ${caption}`)
                    if(typeof pause === 'undefined') {
                        pause = 0
                    }
                    this.pause(pause)
                    .then(result => {
                        return resolve()
                    })
                })
            }
        })
    }

    eraseAnimation(done) {
        this.captionEl.html(this.caption.substr(0, this.captionLength--));
        if(this.captionLength >= 0) {
            setTimeout(this.eraseAnimation.bind(this), this.interval, done)
        } else {
            this.captionLength = 0;
            this.caption = '';
            done()
        }
    }
}

function cursorAnimation() {
    $('#cursor').animate({
        opacity: 0
    }, 'fast', 'swing').animate({
        opacity: 1
    }, 'fast', 'swing');
}

function getData(path, data) {
    return new Promise((resolve, reject) => {
        fetch(`${window.location.href}${path}?${encodeQuery(data)}`,{method:"GET"})
        .then(response => {
            if(response.status == 200) {
                return response.json()
            } else {
                console.warn(`request not successfull ${response.status} ${response.status.text} (${response.url})`)
                return {}
            }
        })
        .then(data => {return resolve(data)})
    })
}

function postData(path, data) {
    return new Promise((resolve, reject) => {
        fetch(`${window.location.href}${path}?${encodeQuery(data)}`,{method:"POST"})
        .then(response => {
            if(response.status == 200) {
                return response.json()
            } else {
                console.warn(`request not successfull ${response.status} ${response.status.text} (${response.url})`)
                return {}
            }
        })
        .then(data => {return resolve(data)})
    })
}

function encodeQuery(data){
    if(!data) {
        return ''
    }
    let query = ""
    for (let d in data)
         query += encodeURIComponent(d) + '=' + 
            encodeURIComponent(data[d]) + '&'
    return query.slice(0, -1)
}

async function findData(coll, value) {
    let data = await getData(coll)
    //console.log(data)
    let result = data.find(element => {
        for(let e in element) {
            if(e != "result" && e != "detail" && value.toUpperCase().includes(element[e])) {
                return true
            }
        }
    })
    return result
}
var hacker = new TypeEffect('#caption', 70)

//postData("users", {name:"Test"})

/*

fetch(`${window.location.href}/users`)
  .then(response => response.json())
  .then(data => console.log(data[0].name));
*/